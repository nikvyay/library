import java.io.IOException;

class DictionaryNumbersLetters extends DictionaryOOP{

    static String regexLine ="\\d{5} [a-zA-Z]{4}\r";
    public DictionaryNumbersLetters(String filename) throws IOException {
        validate();
        DictionaryOOP.dict = DictionaryOOP.createdict(filename);
        //Main.dict1 = dict;
    }

    protected void validate() {
        DictionaryOOP.regexKey = regexLine;
    }

}
