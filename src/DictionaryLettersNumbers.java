import java.io.IOException;

public class DictionaryLettersNumbers extends DictionaryOOP{

    static String regexLine ="[a-zA-Z]{4} \\d{5}\r";
    public DictionaryLettersNumbers(String filename) throws IOException {
        validate();
        DictionaryOOP.dict = DictionaryOOP.createdict(filename);
        //Main.dict1 = DictionaryOOP.dict;
    }

    protected void validate() {
        DictionaryOOP.regexKey = regexLine;
    }

}