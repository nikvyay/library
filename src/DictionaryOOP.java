import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.FileWriter;
import java.util.*;
import java.lang.Math;

abstract public class DictionaryOOP {
    protected static HashMap<String, String> dict = new LinkedHashMap();

    protected static  int examination = 0;

    public static  String regexKey;

    protected abstract void validate ();


    static boolean flag = false;
    public static void getDefinition() {
        System.out.println("Введите ключ:");
        String word;
        Scanner scanner3 = new Scanner(System.in);
        word = scanner3.nextLine();
        if (dict.containsKey(word)) {
            System.out.println("Запись:");
            String definition = dict.get(word);
            System.out.println(word + " " + definition);
            flag = true;
        } else {
            System.out.println("Данная запись отсутсвует в словаре");
        }
    }


    public static void printAllWords(String file) {
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(file)))
        {
            while ((line = br.readLine()) != null) {
                System.out.print(line);
                System.out.print("\n");
            }

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }

    }

    public static void printKeyValue() {
        System.out.println("Содержимое словаря:");
        for (HashMap.Entry<String, String> entry : dict.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public static void addKey(String file) throws IOException {
        System.out.println("Введите запись:");
        String word;
        Scanner scanner5 = new Scanner(System.in);
        word = scanner5.nextLine();
        String word1 = word + "\r";
        if ((word1.matches(regexKey)))
        {
            int index = word.indexOf(" ");
            String number = word.substring(0, index);
            String character = word.substring(index + 1);
            if (!dict.containsKey(number))
            {

                if (index != -1)
                {
                    dict.put(number, character);
                    BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
                    writer.write(number + " " + character);
                    writer.newLine();
                    writer.close();
                }

            }
            else {System.out.println("В словаре есть такой ключ");}
            if (dict.containsKey(word)) {
                System.out.println("Ключ " + word + " уже существует в словаре.");
            }


        }
        else{
            System.out.println("Запись не прошла валидацию");
        }
    }

    public static int workMain(String filename)
    {
        try {
            System.out.println("Выберите словарь:");
            System.out.println("1 - Буквы-Цифры");
            System.out.println("2 - Цифры-Буквы");
            Scanner choose = new Scanner(System.in);
            int number = choose.nextInt();
            switch (number) {
                case 1:
                    examination = number;
                    dict.clear();
                    new DictionaryLettersNumbers(filename);
                    return number;
                case 2:
                    examination = number;
                    dict.clear();
                    new DictionaryNumbersLetters(filename);
                    return number;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            System.out.print("Вы ввели некорректный путь к файлу!");
        }
        return 0;
    }

    public static HashMap createdict(String filename) throws IOException
    {
        Path path = Paths.get(filename);
        Scanner scanner = new Scanner(path);
        HashMap map = dict;
        scanner.useDelimiter("\n");
        while (scanner.hasNext())
        {
            map =check_type(scanner.next(),map);
        }
        scanner.close();
        return map;
    }

    private static HashMap check_type(String line, HashMap hmap)
    {
        if((line.matches(regexKey)))
        {
            if (hmap.containsKey(line))
            {
                System.out.println("Такая запись уже есть в словаре: "+line);
            }
            else {
                int index = line.indexOf(" ");
                String number = line.substring(0, index);
                String character = line.substring(index + 1);
                hmap.put(number, character);
                //System.out.println("Добавлена запись:" + number + " " + character);
            }
        }
        return  hmap;
    }
    public static void removeWord(String file){
        System.out.println("Введите ключ:");
        String word;
        Scanner scanner4 = new Scanner(System.in);
        word = scanner4.nextLine();
        word =word.trim();
        if (dict.containsKey(word))
        {
            String valueKey = dict.get(word);
            dict.remove(word);
            deleteLineInFile(word, valueKey, file);
            System.out.println("Значение удалено успешно");
        }
        else System.out.println("Данная запись отсутствует в словаре");
    }

    private static void deleteLineInFile(String key, String value,String filename) {
        File inputFile = new File(filename);
        File tempFile = new File(inputFile.getParent() + "\\temp.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {
            String lineToRemove = key + " " + value;
            String currentLine;
            boolean flag = false;

            while ((currentLine = reader.readLine()) != null) {
                String currentLine1 = currentLine + "\r";
                if (currentLine1.equals(lineToRemove) && !flag) {
                    flag = true;
                    continue;
                }
                writer.write(currentLine + System.lineSeparator());
            }
            if (!flag) {
                System.out.println("Не нашли нужной строчки");
                return;
            }
            writer.close();
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        if (inputFile.delete())
        {
            if (!tempFile.renameTo(inputFile)) {
                System.out.println("Не удалось переименовать временный файл");
            }
        } else
        {
            System.out.println("Не удалось удалить исходный файл");
        }
    }
}
