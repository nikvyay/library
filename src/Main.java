import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
    //static DictionaryLettersNumbers dict = new DictionaryLettersNumbers();
    private static String file;

    public static void main(String[] args) throws IOException {
        try {
            System.out.println("Введите путь к словарю:");
            Scanner scanner = new Scanner(System.in);
            String inputtext = scanner.nextLine();
            //"C:\\Users\\nikbo\\Desktop\\AA.txt";
            File f = new File(inputtext);
            if (f.exists())
                file = inputtext;

            else
                throw new Exception();
        } catch (Exception e) {
            System.out.println("Путь введён некорректно");
            return;
        }
        DictionaryOOP.workMain(file);


        while (true) {
            System.out.println("Выбирите действие:");
            System.out.println("1. Просмотр содержимого выбранного словоря");
            System.out.println("2. Просмотр содержимого всего файла");
            System.out.println("3. Поиск записи по ключу");
            System.out.println("4. Удаление записи по ключу");
            System.out.println("5. Доблавение записи");
            System.out.println("6. Выбор словаря");
            System.out.println("0. Выход");
            int console = -1;

            Scanner scannerconsole = new Scanner(System.in);
            console = scannerconsole.nextInt();
            switch (console) {
                case 6:
                    DictionaryOOP.workMain(file);
                    break;
                case 5:
                    DictionaryOOP.addKey(file);
                    break;
                case 4:

                    DictionaryOOP.removeWord(file);
                    break;
                case 3:
                    DictionaryOOP.getDefinition();
                    break;
                case 2:
                    DictionaryOOP.printAllWords(file);
                    break;
                case 1:
                    DictionaryOOP.printKeyValue();
                    break;
                case 0:
                    return;
            }
        }
    }
}